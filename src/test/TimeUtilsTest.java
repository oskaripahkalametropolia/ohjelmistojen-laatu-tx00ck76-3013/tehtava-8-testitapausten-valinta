package test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import time_utils.TimeUtils;

public class TimeUtilsTest {	
	@ParameterizedTest(name="[{index}] {0} seconds is {1}")
	@CsvFileSource(resources = "/time.csv")
	public void testTimeToSec(int sec, String time){
		assertEquals(sec, TimeUtils.timeToSec(time));
	}

	@ParameterizedTest(name="[{index}] {1} is {0} seconds")
	@CsvFileSource(resources = "/time.csv")
	public void testSecToTime(int sec, String time){
		assertEquals(time, TimeUtils.secToTime(sec));
	}
}
